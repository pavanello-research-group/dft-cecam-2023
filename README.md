# DFT CECAM 2023


This is Michele's talk at the 
**Accelerating Improvements in Density Functional Theory** August 21, 2023 - August 25, 2023, CECAM HQ, Lausanne, CH

To run this notebook, you need to install jupyter notebooks:
`pip install notebook`

You also need to install DFTpy and QEpy:
`pip install dftpy qepy`

Good luck, and feel free to reach out to me at m.pavanello@rutgers.edu or @MikPavanello on X.
